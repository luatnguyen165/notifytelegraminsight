const _ = require('lodash');
const { bot } = require('../bot/bot.bot');
module.exports = async function(ctx){
	const response = {
		code: 1001,
		message:'Thất bại'
	};
	try {
		const {message} = ctx.params;
		if(message){
			await this.broker.call('v1.notifyTelegramInsightModel.create',[{
				message: `${message}`
			}]);
		}
	
	
	} catch (error) {
		if(error.message === 'MoleculerError'){
			this.logger.error('[NotifyTelegramInsightVip ErrorMoleculer]',JSON.stringify(error));
			response.message = 'Internal Server Error';
			return response;
		}
	}
};