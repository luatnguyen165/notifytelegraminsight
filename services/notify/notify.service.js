'use strict';

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */
module.exports = {
	name: 'notifyTeletegramInsight',
	version: 1,
	mixins: [],
	/**
     * Settings
     */
	settings: {

	},

	/**
     * Dependencies
     */
	dependencies: [],

	/**
     * Actions
     */
	actions: {
		notifyTelegramInsightVip: {
			params:{
				message: 'string|required'
			},
			handler: require('./actions/notifyTelegramInsightVip.action')
		}
	},

	/**
     * Events
     */
	events: {

	},

	/**
     * Methods
     */
	methods: {
		telegramBot: require('./method/telegramBot.method')
	},

	/**
     * Service created lifecycle event handler
     */
	async created() {
		await this.telegramBot();
	},

	/**
     * Service started lifecycle event handler
     */
	async started() {
	
	},

	/**
     * Service stopped lifecycle event handler
     */
	async stopped() {

	}

};