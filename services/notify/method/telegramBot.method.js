const _ = require('lodash');
const { bot } = require('../bot/bot.bot');
const moment = require('moment');

module.exports = async function() {
	Logger = this.logger.info;
	const response = {
		code: 9999,
		message: 'TeleEmployee kết nối thất bại vui lòng kết nối lại sau.'
	};

	try {
		bot.onText(/\/start/, (ctx) => {
			const chatId = ctx.chat.id;
			bot.sendMessage(chatId, 'Xin chào bạn đã đến với hệ thống check lỗi Payme Insight');
		});
			
		bot.on('message', async ctx => {
			const chatId = ctx.chat.id;
			if(ctx.text == '/start'){
			}else if(ctx.text === '/error'){
				try {
					const data =  await this.broker.call('v1.notifyTelegramInsightModel.findMany',[{},'-_id',{
						skip: 0,
						limit: 10,
						sort:{
							createdAt:-1
						}
					}]);
					_.forEach(data,(item)=>{
						bot.sendMessage(chatId,`${item.message} - Thời gian- ${moment(item.createdAt).format('DD/MM/YYYY hh:mm:ss')}`);
					});
				} catch (error) {
					bot.sendMessage(chatId, 'Có lỗi xảy ra vui lòng đợi trong giây lát');
					Logger('[NotifyTelegramInsight] Internal Server Error', JSON.stringify(error));
					if (error === 'MoleculerError') {
						response.code = error.code;
						response.message = error.message;
					}
					return response;
				}
			}else{
				bot.sendMessage(chatId, 'Vui lòng nhập đúng keywords để check lỗi nhé');
			}
		
		});
	
	} catch (error) {
		Logger('[TeleEmployee bot] Kết nối thất bại error', JSON.stringify(error));
		if (error === 'MoleculerError') {
			response.code = error.code;
			response.message = error.message;
		}
		return response;
	}


};